//jshint esversion: 6

//maxWord
let maxWord = (s) => {
  let sArray = s.split(" ");
  let biggestWord = "";
  let bwLength = 0;
  for(var i=0;i<sArray.length;i++){
    if (sArray[i].length > bwLength){
      biggestWord = sArray[i];
      bwLength = sArray[i].length;
    }
  }
  return biggestWord;
};
//maxWord("Learning Web App Development");

//maxword2 same as maxWord but use a forEach instead
let maxWord2 = (s) => {
  let sArr = s.split(" ");
  let biggestWord = "";
  let bwLength = 0;
  sArr.forEach(element => {
    if(element.length > bwLength){
      biggestWord = element;
      bwLength = element.length;
    }
  });
  return biggestWord;
};

//sumEveryNth
let sumEveryNth = (arr,n) => {
  let sum = 0;
  let index = 1;
  arr.forEach(element => {
    if(index % n == 0){
      sum = sum + element;
    }
    index++;
  });
  return sum;
};
//sumEveryNth([2, 3, 5, 7, 11, 13], 2);

//findLast element divisible by 3 or 5
let findLast = arr => {
  last = 0;
  arr.forEach(element => {
    if((element%3 == 0) || (element%5 ==0)){
      last = element;
    }
  });
  return last;
};

//findLastIndex of element divisible by 3 AND 5
let findLastIndex = arr => {
  let lastIdx = 0;
  for(i=0;i<arr.length;i++){
    if((arr[i] % 3 == 0) && (arr[i] % 5 == 0)){
      lastIdx = i;
    }
  }
  return lastIdx;
};

let encrypt = s => {
  let alph = "abcdefghijklmnopqrstuvwxyzaABCDEFGHIJKLMNOPQRSTUVWXYZA"
  let encryptedStr = ""
  for(var i=0;i<s.length;i++){
    let curIdx = alph.indexOf(s[i]);
    encryptedStr += alph[curIdx+1];
  }
  return encryptedStr;
};
